import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService, parseDate } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { TimeSheetService } from '../../services/time-sheet.service';

@Component({
  selector: 'app-time-sheet',
  templateUrl: './time-sheet.component.html',
  styleUrls: ['./time-sheet.component.scss'],
})
export class TimeSheetComponent implements OnInit {
  timeSheetFormGroup: FormGroup;
  timeSheetList = [];
  submitted: boolean;

  modalRef: BsModalRef;
  // modal config to unhide modal when clicked outside
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-lg',
  };
  constructor(
    private timeSheetService: TimeSheetService,
    private modalService: BsModalService,
    private _fb: FormBuilder,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getTimeSheet();
    this.calculateWeeksDays();
  }

  weekday = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

  weeksDays = [];

  /**To Calculate the Weekdays form currnet date */
  calculateWeeksDays(): void {
    const date = new Date();
    let days = date.getDay();

    /**To calculate Starting of week */
    const remDaystofWeek = 6 - days;
    const startofWeek = 6 - remDaystofWeek - 1; //To Customize Monday as a start of week

    const startDay = date.getDate() - startofWeek;

    const startDate = new Date(
      date.getTime() - startofWeek * 24 * 60 * 60 * 1000
    );

    for (let i = 0; i <= 6; i++) {
      this.weeksDays.push({
        date: startDay + i,
        days: this.weekday[i],
        noOfday: startDate.getDate() + i,
        fullDate: new Date(date.getTime() + i * 24 * 60 * 60 * 1000),
        class: i === days ? 'selectedDay' : '',
      });
    }
  }

  getTimeSheet(): void {
    this.timeSheetService.getTimeSheet().subscribe((response) => {
      this.timeSheetList = response;
    });
  }

  /*  @buildTimeSheetFormGroup() Build Time Sheet Add Froms Using Reactive Forms

*/

  buildTimeSheetFormGroup(): void {
    this.timeSheetFormGroup = this._fb.group({
      name: ['', Validators.required],
      Date1: [null],
      Date2: [null],
      Date3: [null],
      Date4: [null],
      Date5: [null],
      Date6: [null],
      Date7: [null],
    });
  }

  get f() {
    return this.timeSheetFormGroup.controls;
  }

  editField: string;

  updateList(id: number, property: string, event: any) {
    const editField = event.target.textContent;
    this.timeSheetList[id][property] = editField;
  }

  changeValue(id: number, property: string, event: any) {
    this.editField = event.target.textContent;

    this.timeSheetService
      .updateTimeSheet(this.timeSheetList[id].id, this.timeSheetList[id])
      .subscribe((response) => {
        this.toastr.success('Time edited Successfully');
      }),
      (error) => {
        this.toastr.success(error);
      };
  }

  remove(id: any) {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.data = this.timeSheetList[id].name;
    this.modalRef.content.action = 'delete';
    this.modalRef.content.onClose.subscribe((confirm) => {
      if (confirm) {
        this.deleteWorkerTimeById(id);
      }
    });
  }

  deleteWorkerTimeById(id): void {
    this.timeSheetService
      .deleteTimeSheet(this.timeSheetList[id].id)
      .subscribe((response) => {
        this.toastr.success(
          'Time of ' + this.timeSheetList[id].name + ' deleted Successfully'
        );
        this.getTimeSheet();
      }),
      (error) => {
        this.toastr.success(error);
      };
  }

  openAddModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
    this.buildTimeSheetFormGroup();
  }

  onCancel(): void {
    this.submitted = false;
    this.modalRef.hide();
    this.timeSheetFormGroup.reset();
  }

  getTotal(time): number {
    var sum = 0;

    for (var key in time) {
      if (key != 'name' && key != 'id') {
        if (time[key] != null) {
          sum += parseFloat(time[key]);
        }
      }
    }
    return sum;
  }

  addTimeSheet(): void {
    this.submitted = true;
    if (this.timeSheetFormGroup.invalid) return;
    this.timeSheetService
      .postTimeSheet(this.timeSheetFormGroup.value)
      .subscribe((response) => {
        this.modalRef.hide();
        this.toastr.success('Time Sheet added Successfully');
        this.getTimeSheet();
        this.submitted = false;
      }),
      (error) => {
        this.toastr.success(error);
      };
  }
}
