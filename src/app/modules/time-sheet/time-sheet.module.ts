import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeSheetRoutingModule } from './time-sheet-routing.module';
import { TimeSheetComponent } from './components/time-sheet/time-sheet.component';
import { DemoMaterialModule } from './material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [TimeSheetComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    TimeSheetRoutingModule,
    DemoMaterialModule,
  ],
  entryComponents: [ConfirmationDialogComponent],
})
export class TimeSheetModule {}
