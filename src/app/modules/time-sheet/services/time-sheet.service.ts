import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TimeSheetService {
  apiURL: any = environment.apiUrl;
  constructor(private http: HttpClient) {}

  getTimeSheet(): Observable<any> {
    return this.http.get(`${this.apiURL}timeSheet`);
  }

  getTimeSheetByID(id) {
    return this.http.get(`${this.apiURL}timeSheet/${id}`);
  }

  postTimeSheet(body): Observable<any> {
    return this.http.post(`${this.apiURL}timeSheet`, body);
  }

  deleteTimeSheet(timeSheetId): Observable<any> {
    return this.http.delete(`${this.apiURL}timeSheet/${timeSheetId}`);
  }

  updateTimeSheet(timeSheetId, body): Observable<any> {
    return this.http.put(`${this.apiURL}timeSheet/${timeSheetId}`, body);
  }
}
