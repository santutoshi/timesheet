import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { OnlyNumbersDirective } from './directives/only-numbers.directive';

@NgModule({
  declarations: [ConfirmationDialogComponent, OnlyNumbersDirective],
  imports: [CommonModule],
  exports: [ConfirmationDialogComponent, OnlyNumbersDirective],
})
export class SharedModule {}
